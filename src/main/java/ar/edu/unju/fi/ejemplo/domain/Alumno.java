package ar.edu.unju.fi.ejemplo.domain;

public class Alumno {
//	dni, nombre, email, domicilio y tel�fono.
	private String nombre;
	private String domicilio;
	private String email;
	private int telefono;
	private int dni;
	
	
	
	public Alumno(String nombre, String domicilio, String email, int telefono, int dni) {
		super();
		this.nombre = nombre;
		this.domicilio = domicilio;
		this.email = email;
		this.telefono = telefono;
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	@Override
	public String toString() {
		return "Alumno [nombre=" + nombre + ", domicilio=" + domicilio + ", email=" + email + ", telefono=" + telefono
				+ ", dni=" + dni + "]";
	}
	
	
	
	
	
	
}