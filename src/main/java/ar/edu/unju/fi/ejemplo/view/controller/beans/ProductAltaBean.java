package ar.edu.unju.fi.ejemplo.view.controller.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ar.edu.unju.fi.ejemplo.domain.Product;

/**
 * 
 * @author jzapana
 *
 */
@ManagedBean
@SessionScoped
public class ProductAltaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Product product;

	public ProductAltaBean(){
		
	}
	
	public String preInsert(){
		System.out.println("******** preInsert");
		setProduct(new Product());
		return "productAlta.xhtml?faces-redirect=true";
	}	
	
	/**
	 * sucede antes de entrar a la pantalla de modificaci�n
	 * @return
	 */
	public String preUpdate(){
		System.out.println("******** preUpdate alta - C�digo: " + getProduct().getCodigo() + " Nombre: "+ getProduct().getNombre());
		return "productAlta.xhtml?faces-redirect=true";
	}	
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public String guardar(){
		System.out.println("...... guardando....");
		//lista.add(product);  //Guardo el objeto en la lista est�tica
		return "productList";
	}
}
