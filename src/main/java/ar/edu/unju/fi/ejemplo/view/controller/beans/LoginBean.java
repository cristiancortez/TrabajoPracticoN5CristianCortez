package ar.edu.unju.fi.ejemplo.view.controller.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

//Login del sistema

@ManagedBean(name="loginBean")
@SessionScoped
public class LoginBean {
	
	private static final long serialVersionUID = 1L;
 //jboss
	private String nombreUsuario;
	private String password;
	private final String FORWARD_DASHBOARD = "home.xhtml?faces-redirect=true";
	
	public String ingresar() throws Exception{
		if(nombreUsuario.isEmpty() || password.isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Debe Ingresar usuario y clave"));
			return null;
		}

		if(nombreUsuario.equals("cristian") && password.equals("cristian123")){
				
				//FIXME deber�a mejorar este c�digo para que sea reusable
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Debe Ingresar usuario y clave"));
				return  FORWARD_DASHBOARD;
			}else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Debe Ingresar usuario y clave correctos"));
				
			}
			
			return null;
	}

	
	public void closeSession()
	{
		
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.invalidate();
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	
}