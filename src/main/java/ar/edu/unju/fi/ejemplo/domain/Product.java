package ar.edu.unju.fi.ejemplo.domain;

import java.util.Date;

/**
 * 
 * @author Jose Zapana
 *
 */
public class Product {

	private Integer producId;
	private Integer codigo;
	private String nombre;
	private Double precioUnitario;
	private Date fechaIngreso;
	
	public Product(){
		
	}
	
	/**
	 * Constructor 
	 * @param producId
	 * @param codigo
	 * @param nombre
	 * @param precioUnitario
	 */
	public Product(Integer producId, Integer codigo, String nombre,
			Double precioUnitario) {
		super();
		this.producId = producId;
		this.codigo = codigo;
		this.nombre = nombre;
		this.precioUnitario = precioUnitario;
		this.fechaIngreso = new Date();
	}
	public Integer getProducId() {
		return producId;
	}
	public void setProducId(Integer producId) {
		this.producId = producId;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(Double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	
	
}
