package ar.edu.unju.fi.ejemplo.view.controller.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import ar.edu.unju.fi.ejemplo.domain.Product;

/**
 * 
 * @author jzapana
 *
 */
@ManagedBean
@SessionScoped
public class ProductListBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8194186654346103799L;
	private Integer codigo;
	private String nombre;
	private List<Product>productList;
	private String pais;
	private String provincia;
	private List<SelectItem> listaProvincias;

	/**
	 * Busca las provincias del pa�s seleccionado y carga la lista <code>listaProvincias</code>
	 */
	public void buscarProvincias(){
		System.out.println("... en este m�todo cargamos la lista de provincias para el pais ingresado:  " + pais);
	}

	public List<Product> find(){
		List<Product>list = new ArrayList<Product>();
		list.add(new Product(1, 1000, "Lampara", 150.30));
		list.add(new Product(2, 1001, "Mesa", 550.00));
		list.add(new Product(3, 1002, "Silla", 200.0));
		list.add(new Product(4, 1003, "Cama", 2500.00));
		list.add(new Product(5, 1004, "Televisor", 5000.00));
		return list;
	}
	

	/**
	 * B�squeda de productos
	 * @return
	 */
	public String search(){
		
		//FIXME aqu� deber�a implementarse la b�squeda con par�metros
		System.out.println("codigo: " + codigo);
		System.out.println("nombre: " + nombre);
		productList = find();
		System.out.println("productList " +  productList.size());
		
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Buscando Productos...", "Se realiz� la b�squeda de productos");
        FacesContext.getCurrentInstance().addMessage(null, message);
		
		return null;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public List<Product> getProductList() {
		return productList;
	}
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public List<SelectItem> getListaProvincias() {
		return listaProvincias;
	}

	public void setListaProvincias(List<SelectItem> listaProvincias) {
		this.listaProvincias = listaProvincias;
	}

	
}
