package ar.edu.unju.fi.ejemplo.view.controller.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ar.edu.unju.fi.ejemplo.domain.Alumno;




@ManagedBean
@SessionScoped

public class AlumnoBean {
	
	private int dni;
	private String nombre;
	private List<Alumno>alumnoList;
	
	
	
	
	public List<Alumno> alumno(){
		ArrayList<Alumno> alumnoList= new ArrayList<Alumno>();
		
		alumnoList.add( new Alumno("cristian", "Undiano 690", "hola@hotmail.com", 420963, 12345));
		alumnoList.add( new Alumno("ema", "Undiano 908", "hola@hotmail.com", 420962, 98765));
		alumnoList.add( new Alumno("marcos", "Undiano 190", "hola@hotmail.com", 420961, 202020));
		return alumnoList;
	}
	
	
	public String bu(){
		//FIXME aqu� deber�a implementarse la b�squeda con par�metros
		alumnoList = alumno();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Buscando Productos...", "Se realiz� la b�squeda de productos");
        FacesContext.getCurrentInstance().addMessage(null, message);
		
		return null;
	}
	
	
	public void guardar(String nombre, String domicilio, String email, int tel, int dni)
	{
		
		alumnoList.add( new Alumno(nombre, domicilio, email, tel, dni));
		
		
		
		
	}
	
	
	
	
	
	 public String buscarNombre(String nombre, List<Alumno> alumnoList )
	    {
	        int contador = 0 ;
	        for (int i=0; i < alumnoList.size(); i++)
	        {
	           if (alumnoList.get(i).getNombre()==nombre)
	           {
	               contador ++;
	               System.out.println("El Alumno: " + nombre + " encontrado");
	           }
	        }
	         if(contador > 1)
	         {
	                System.out.print("No hay nada" + "\n");
	         }
	        
	        return null;
	    }
	
	
	public List<Alumno> getAlumnoList() {
		return alumnoList;
	}


	public void setAlumnoList(List<Alumno> alumnoList) {
		this.alumnoList = alumnoList;
	}


	public int getDni() {
		return dni;
	}


	public void setDni(int dni) {
		this.dni = dni;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	
	
	
	

}
