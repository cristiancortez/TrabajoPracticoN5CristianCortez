package ar.edu.unju.fi.ejemplo.view.controller.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 * 
 * @author jzapana
 *
 */
@ManagedBean
@SessionScoped
public class EjemploAjaxBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8194186654346103799L;

	private String pais;
	private String provincia;
	private List<SelectItem> listaProvincias;

	/**
	 * Busca las provincias del pa�s seleccionado y carga la lista <code>listaProvincias</code>
	 */
	public void buscarProvincias(){
		System.out.println("Cargando provicias para el pa�s:  " + pais);
		listaProvincias = cargarProvincias(pais);
	}
	
	/**
	 * Busca las provincias para el pa�s seleccionado
	 * @param pais
	 * @return
	 */
	private List<SelectItem> cargarProvincias(String pais) {
		List<SelectItem> lista = new ArrayList<SelectItem>();
		
		// Deber�a buscar de una base de datos en un caso real
		if(pais.equals("ARG")) {
			lista.add(new SelectItem("BUE", "Buenos Aires"));
			lista.add(new SelectItem("JUJ", "Jujuy"));
			lista.add(new SelectItem("COR", "Cordoba"));
		}else if(pais.equals("BRA")) {
			lista.add(new SelectItem("SAO", "San Pablo"));
			lista.add(new SelectItem("RIO", "Rio de Janeiro"));
			lista.add(new SelectItem("FLO", "Floreanapolis"));
		}else if(pais.equals("BOL")) {
			lista.add(new SelectItem("LAP", "La paz"));
			lista.add(new SelectItem("COCH", "Cochabamba"));
			lista.add(new SelectItem("TAR", "Tarija")); 
		}
		
		System.out.println("provincias: " + lista.size());
		for(SelectItem p: lista) {
			System.out.println("Provincia: " + p.getValue() + "-" + p.getLabel());
		}
		return lista;
		
		
	}
	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public List<SelectItem> getListaProvincias() {
		return listaProvincias;
	}

	public void setListaProvincias(List<SelectItem> listaProvincias) {
		this.listaProvincias = listaProvincias;
	}

	
}
